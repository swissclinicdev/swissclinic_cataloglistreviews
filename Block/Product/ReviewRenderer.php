<?php

namespace Swissclinic\Theme\Magento_Review\Block\Product;

class ReviewRenderer extends \Magento\Review\Block\Product\ReviewRenderer
{
    public function getReviewsSummaryHtml(
        \Magento\Catalog\Model\Product $product,
        $templateType = self::DEFAULT_VIEW,
        $displayIfNoReviews = false
    ) {
        if (!$product->getRatingSummary()) {
            $this->_reviewFactory->create()->getEntitySummary($product, $this->_storeManager->getStore()->getId());
        }

        if (!$product->getRatingSummary() && !$displayIfNoReviews) {
            return '';
        }

        $this->setTemplate($templateType);

        $this->setDisplayIfEmpty($displayIfNoReviews);

        $this->setProduct($product);

        return $this->toHtml();
    }
}
